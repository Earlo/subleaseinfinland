module ApplicationHelper
  def session_path(scope)
    new_user_session_path
  end

  def cache_data(uniq_key, usage_data, expire, force)
    if REDIS.exists(uniq_key) && !force
      return eval(REDIS.get(uniq_key))
    else
      REDIS.set(uniq_key, usage_data)
      REDIS.expire(uniq_key, expire)
      return usage_data
    end
  end

  def cache_cities(force=false)
    u_key = "SOSO:CITIES"
    if !force && REDIS.exists(u_key)
      return eval(REDIS.get(u_key))
    else
      cities = City.all.as_json
      REDIS.set(u_key,cities)
      REDIS.persist(u_key)
      return cities
    end
  end

  def flash_class(level)
    case level.to_sym
    when :notice then "alert alert-success"
    when :info then "alert alert-info"
    when :alert then "alert alert-danger"
    when :warning then "alert alert-warning"
    when :success then "alert alert-success"
    end
  end

  # mailbox
  def active_page(active_page)
    @active == active_page ? "active" : ""
  end

end
