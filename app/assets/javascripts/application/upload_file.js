function readURL(input) {
  $('.property-pictures.preview .preview-container').html("");
  var i;
  for (i=0; i<input.files.length; i++) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('.property-pictures.preview .preview-container').append("<img src='" + e.target.result + "' >");
    }
    reader.readAsDataURL(input.files[i]);
  }
}
