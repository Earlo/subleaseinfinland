var fixbs = function() {
  $('.pp-dropdown.keep-open').on({
      "shown.bs.dropdown": function() { this.closable = false; },
      "click":             function() { this.closable = true; },
      "hide.bs.dropdown":  function() { return this.closable; }
  });
  $('.pp-dropdown-toggle').on('click', function (event) {
      $(this).parent().toggleClass('open');
  });
  $(document).on('click', 'span.month, th.next, th.prev, th.switch, span.year, td.day', function (e) {
    e.stopPropagation();
  });
};
