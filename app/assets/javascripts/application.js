// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require twitter/bootstrap
//= require jquery_ujs
//= require jquery-ui/autocomplete
//= require jquery-ui/slider
//= require bootstrap-datepicker.min
//= require bootstrap-datepicker.en-GB.min
//= require bootstrap-maxlength.min.js
//= require turbolinks
//= require typeahead.bundle
//= require underscore
//= require gmaps/google
//= require jstorage
//= require sisyphus
//= require jquery.geocomplete.min
//= require bootstrap.js.coffee
//= require select2.full.min
//= require_tree ./application

