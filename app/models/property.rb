class Property < ActiveRecord::Base
  belongs_to :user
  has_many :property_pictures
  accepts_nested_attributes_for :property_pictures

  validates :city,:address, presence: true
  validates :address,length: {maximum: 55}
  def self.search_by filter
    filter = process_filter(filter)
    query = ""
    option = {}
    if filter[:city].blank?
      return []
    else
      option[:city] = filter[:city]
      query += "city = :city "
    end
    if !filter[:type].blank?
      option[:type] = filter[:type]
      query += "AND type IN :type"
    end
    if !filter[:min_area].blank?
      option[:min_area] = filter[:min_area]
      query += "AND area >= :min_area "
    end
    if !filter[:max_area].blank?
      option[:max_area] = filter[:max_area]
      query += "AND area <= :max_area "
    end
    if !filter[:min_rent].blank?
      option[:min_rent] = filter[:min_rent]
      query += "AND rent >= :min_rent "
    end
    if !filter[:max_rent].blank?
      option[:max_rent] = filter[:max_rent]
      query += "AND rent <= :max_rent "
    end
    if !filter[:period_start].blank?
      option[:period_start] = DateTime.parse(filter[:period_start])
      query += "AND period_start <= :period_start "
    end
    if !filter[:period_end].blank?
      option[:period_end] = DateTime.parse(filter[:period_end])
      query += "AND period_end >= :period_end "
    end
    if !filter[:guest].blank?
      option[:guest] = filter[:guest]
      query += "AND guest >= :guest "
    end
    if !filter[:address].blank?
      option[:address] = "%#{filter[:address]}%"
      query += "AND address LIKE :address"
    end

    properties = Property.where(query,option)
  end

  private

  def self.process_filter filter
    set = ["select","update","delete","create"]
    if set.any? { |op| filter =~ /#{op}/i }
      return ""
    else
      return filter.symbolize_keys
    end
  end

end
