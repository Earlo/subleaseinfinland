class PropertyPicture < ActiveRecord::Base
   mount_uploader :picture, PictureUploader
   belongs_to :property
end
