class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :registerable, #:database_authenticatable,
    :rememberable, :trackable,# :validatable, :recoverable,
    :omniauthable, :omniauth_providers => [:facebook, :google_oauth2, :twitter]

  acts_as_messageable

  has_many :properties

  def self.search(name = nil)

  end

  def mailboxer_name
    self.name
  end

  def mailboxer_email(object)
    self.email
  end

  def self.from_omniauth(auth)
    user = where(email: auth.info.email).first_or_create do |user|
      # user.send("#{auth.provider[0]}_uid") = auth.uid
      user.email = auth.info.email
      user.name = auth.info.name   # assuming the user model has a name
      user.image = auth.info.image + (auth.provider == "facebook" ? "?type=large" : "")  # assuming the user model has an image
      user.first_name = auth.info.first_name
      user.last_name = auth.info.last_name
    end
    user.update_attributes({"#{auth.provider[0]}_uid".to_sym => auth.uid}) \
      if user.send("#{auth.provider[0]}_uid").blank?
    # update token
    user.update_attributes({"#{auth.provider[0]}_access_token".to_sym => auth.credentials.token})
    user
  end

    # def self.new_with_session(params, session)
    # super.tap do |user|
    # if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
    # user.email = data["email"] if user.email.blank?
    # end
    # end
    # end

end
