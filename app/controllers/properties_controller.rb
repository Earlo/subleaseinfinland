class PropertiesController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => :create
  before_action :authenticate_user!, :only => [:edit, :update, :create]

  def index
    @city = params[:city]||session[:city]
    session[:city] = @city
    @view = params[:view] || "grid"
    respond_to do |format|
      format.html
      format.js {
        @properties = Property.search_by(city: @city).order("created_at DESC").paginate(:per_page => APP_CONFIG["pagination"]["per_page"], :page => params[:page])
        #paginate
      }
    end
  end

  def new
    respond_to do |format|
      format.html
    end
  end

  def edit
    @property = Property.find(params[:id])
    respond_to do |format|
      format.html
    end
  end

  def create
    property = current_user.properties.new(property_params)
    if property.save
      #nested attributes ???? WTF !!!!
      unless params[:property_pictures].blank?
        params[:property_pictures]['picture'].each do |a|
          property_picture = property.property_pictures.create!(:picture => a)
        end
      end
      respond_to do |format|
        format.js {
          render :js => "window.location = '#{edit_property_path(property)}'"
        }
        format.html {
          redirect_to property
        }
      end
    else
      respond_to do |format|
        format.js {
        }
      end
    end
  end

  def update
    @property = Property.find(params[:id])
    if @property.update_attributes(property_params)
      #nested attributes ???? WTF !!!!
      unless params[:property_pictures].blank?
        @property.property_pictures.destroy_all
        params[:property_pictures]['picture'].each do |a|
          property_picture = @property.property_pictures.create!(:picture => a)
        end
      end
      respond_to do |format|
        format.html {
          redirect_to @property
        }
      end
    else
      respond_to do |format|
        format.html {
          render 'edit'
        }
      end
    end
  end

  def search_by
    @city = params[:city]||session[:city]
    respond_to do |format|
      format.js {
        # processing here
        params[:filter][:city] = @city
        params[:filter].each {|key,val| params[:filter].delete(key) if params[:filter][key].empty?}
        @properties = Property.search_by(params[:filter])
        #.paginate(:per_page => APP_CONFIG["pagination"]["per_page"], :page => params[:page]) rescue []
        #paginate
      }
    end
  end

  def show
    @property = Property.find(params[:id])
    @property_pictures = @property.property_pictures
    @user = @property.user
    @city = cache_cities(true).select{|m| m['name'].downcase == @property.city.downcase}.first
  end

  private

  def property_params
    # params.require(:property).permit!
    unless params[:property].blank?
      params.require(:property).permit(
        :user_id,
        :property_type,
        :area,
        :rent,
        :period_start,
        :period_end,
        :country,
        :city,
        :address,
        :description,
        :quantity,
        property_pictures_attributes: PropertyPicture.attribute_names.collect { |att| att.to_sym}
      )
    end
  end

  end

