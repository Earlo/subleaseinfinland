class UsersController < ApplicationController
  before_action :authenticate_user!

  def show
    @user = User.find(params[:id])
    @properties = @user.properties
    @view = params[:view] || "grid"
    # TODO ajax will go here
  end

  def search_by_name
    # this returns a hash of search results
    if !params[:name].blank?
      users = User.where('name LIKE ?', "%#{params[:name]}%")
    else
      users = []
    end
    render json: { users: users.to_json }
  end

end


