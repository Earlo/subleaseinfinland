class WelcomeController < ApplicationController
  layout 'welcome_layout'

  skip_before_action :authenticate_user!

  def welcome
    @majors = MAJOR
    @cities = cache_cities.map{|m| m['name']}
    respond_to do |format|
      format.html
    end
  end

end
