class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name
      t.float :longitude
      t.float :latitude
      t.string :postal_code

      t.timestamps null: false
    end
    add_index :cities, [:name], :unique => true
  end
end
