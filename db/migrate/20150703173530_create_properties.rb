class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.integer :user_id
      t.string :property_type
      t.float :area
      t.float :rent
      t.integer :guest,default: 1
      t.datetime :period_start
      t.datetime :period_end
      t.string :country
      t.string :city
      t.string :address
      t.text :description
      t.string :quantity

      t.timestamps null: false
    end
  end
end
