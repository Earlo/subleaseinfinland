class CreatePropertyPictures < ActiveRecord::Migration
  def change
    create_table :property_pictures do |t|
      t.integer :property_id
      t.string :picture
    end
  end
end
