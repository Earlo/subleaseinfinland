# encoding: UTF-8

require 'csv'
require 'faker'


namespace :events do
  def fix_encode data
    if ! data.valid_encoding?
      data = data.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
    end
    data
  end
  task :fetch => :environment do

    data_file = Rails.root.join("vendor","fi_postal_codes.csv")
    data = {}
    content = File.open(data_file, "r:UTF-8") do |f|
      f.read
    end
    begin
      CSV.parse(fix_encode(content),headers: true).each do |row|
        begin
          if row.inspect.valid_encoding?
            city_name = fix_encode(row["Place Name"])
            if data[city_name].blank?
              data[city_name] = {"postal_code" => fix_encode(row["Postal Code"]),
                                 "longitude" => fix_encode(row["Longitude"]),
                                 "latitude" => fix_encode(row["Latitude"])}
            else
            end
          end
        rescue => e
          puts row
        end
      end
    rescue => e
    end
    data.each do |key,val|
      begin
        City.create(name: key,postal_code: val['postal_code'],longitude: val['longitude'],latitude: val['latitude'])
      rescue => e
      end
    end
    if true
      10.times do |i|
        property = Property.create(user_id: 1,
                       property_type: "Appartment",
                       area: rand(10) + 1,
                       rent: rand(100) + 1,
                       guest: rand(4) +1,
                       period_start: DateTime.now - rand(10).days,
                       period_end: DateTime.now + rand(10).days,
                       country: "Finland",
                       city: MAJOR.sample,
                       address: Faker::Address.street_address,
                       description: Faker::Lorem.sentence(3, true),
                       quantity: ["day","month","week","all"].sample
                       )
        property.property_pictures.create!(remote_picture_url: "http://lorempixel.com/400/400/city/")
      end
      10.times do |i|
        property = Property.create(user_id: 1,
                                   property_type: "Appartment",
                                   area: rand(10) + 1,
                                   rent: rand(100) + 1,
                                   period_start: DateTime.now - rand(10).days,
                                   period_end: DateTime.now + rand(10).days,
                                   country: "Finland",
                                   city: "Helsinki",
                                   address: Faker::Address.street_address,
                                   description: Faker::Lorem.sentence(3, true),
                                   quantity: ["day","month","week","all"].sample
                                  )
        property.property_pictures.create!(remote_picture_url: "http://lorempixel.com/400/400/city/")
      end
    end
  end

end
